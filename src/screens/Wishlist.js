import React from 'react';
import {View, Text} from 'react-native';
import { Input } from 'react-native-elements';

export default function Wishlist() {
  return (
    <View style={{flex: 1}}>
      <View style={{flex: 1, marginTop: 16}}>
        <Input 
          placeholder='Search'
          underlineColorAndroid='transparent'
          inputContainerStyle={{borderBottomWidth: 0}}
          style={{borderWidth: 1, borderRadius: 12, }}
          />
      </View>
    </View>
  );
}
