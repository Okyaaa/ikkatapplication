import React, {Component, useState, useEffect, useContext} from 'react';
import {View, StyleSheet, Text, Dimensions, Image, ScrollView, TouchableOpacity} from 'react-native';
import {Input} from 'react-native-elements/dist/input/Input';
import Icon from 'react-native-vector-icons/Ionicons';
import MenuButton from '../components/MenuButton';
import ProductHome from '../components/productHome';
import {BASE_URL, BASE_URL_PROD, XSignature} from '../config';
import {COLORS, images, theme} from '../constants';
import axios from 'axios';
import {useNavigation} from '@react-navigation/native';

const {banner1, banner2, banner3} = images;

const imagesBanner = [
  {
    img: banner1,
  },
  {
    img: banner2,
  },
  {
    img: banner3,
  },
];

const {SIZES} = theme;

export default function Home({navigation}) {
  const [imgActive, setImageActive] = useState(0);
  const [imgActive2, setImageActive2] = useState(0);
  const [productList, setProductList] = useState([]);
  const [selectedProduct, setSelectedProduct] = useState(null);

  const getDataProduct = async () => {
    axios
      .post(
        `${BASE_URL_PROD}/service/product/catalog`,
        {
          source: 'WEB',
          sourceDesc: 'cent.com',
          categoryIds: ['ALL'],
          orderBy: 'name',
          orderType: 'desc',
          maxResult: '7',
          startRow: '0',
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'X-Signature': XSignature,
          },
        },
      )
      .then(response => {
        const data = response.data.productList;
        console.log(`reponse api 1 =>>  ${JSON.stringify(data)}`);
        console.log(data);
        setProductList(data);
      })
      .catch(e => {
        alert(e.message);
      });
  };

  useEffect(() => {
    getDataProduct();
  }, []);

  const onChange = nativeEvent => {
    if (nativeEvent) {
      const slide = Math.ceil(
        nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width,
      );
      if (slide != imgActive) {
        setImageActive(slide);
      }
    }
  };

  const onChangeSecond = nativeEvent => {
    if (nativeEvent) {
      const slide = Math.ceil(
        nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width,
      );
      if (slide != imgActive2) {
        setImageActive2(slide);
      }
    }
  };

  const onSelectProduct = id => {
    // setSelectedProduct(id);
    // console.log(selectedProduct);
    // navigation.navigate('DetailProduct', {
    //   productId: selectedProduct,
    // });

    navigation.navigate('DetailProduct', {
      screen: 'Home',
      params: {productId: id},
    });
    // navigation.navigate('DetailProduct', {
    //   params: { productId: selectedProduct },
    // });
    // navigation.navigate('DetailProduct');
  };

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <ScrollView key={1}>
        <View style={styles.topBar}>
          <View style={styles.topbarComponent}>
            <Input
              placeholder="Search"
              underlineColorAndroid="transparent"
              inputContainerStyle={{borderBottomWidth: 0}}
              leftIcon={<Icon name="search" size={22} />}
            />
          </View>
          <Icon name="book" size={22} />
          <Icon name="notifications" size={22} />
        </View>
        <View style={styles.bannerWrapper}>
          <ScrollView
            key={2}
            onScroll={({nativeEvent}) => onChange(nativeEvent)}
            showsHorizontalScrollIndicator={false}
            pagingEnabled
            horizontal
            style={styles.bannerScroll}>
            {imagesBanner.map((e, index) => (
              <Image
                key={e}
                style={styles.bannerContent}
                source={e.img}></Image>
            ))}
          </ScrollView>
          <View style={styles.wrapDot}>
            {imagesBanner.map((e, index) => (
              <Text
                key={e}
                style={
                  imgActive == index ? styles.dotActive : styles.dotInActive
                }>
                ●
              </Text>
            ))}
          </View>
        </View>
        <View style={styles.balanceWrapper}>
          <View>
            <Text style={styles.balanceText}>Hi, Dian</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.balanceText}>Saldo total </Text>
            <Text style={styles.balanceMoneyText}> Rp500.000 ></Text>
          </View>
        </View>
        <View style={styles.balanceWrapper2}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View>
              <Text style={styles.UserText}>Pinjamanku </Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.balanceMoneyText}> Rp500.000 </Text>
              <Text style={styles.UserText}>Lihat Detail ></Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View>
              <Text style={styles.UserText}>Simpananku </Text>
            </View>
            <View style={{flexDirection: 'row', paddingLeft: 4}}>
              <Text style={styles.balanceMoneyText}> Rp500.000 </Text>
              <Text style={styles.UserText}>Lihat Detail ></Text>
            </View>
          </View>
        </View>
        <View style={styles.buttonWrapper}>
          <MenuButton
            color="#F2913D"
            name="phone"
            title="PULSA"
            iconColor="white"
          />
          <MenuButton
            color="#9681F7"
            name="cellphone-nfc"
            title="PAKET DATA"
            iconColor="white"
          />
          <MenuButton
            color="#45B5F8"
            name="water"
            title="PDAM"
            iconColor="white"
          />
          <MenuButton
            color="#FCD84A"
            name="flash"
            title="LISTRIK"
            iconColor="white"
          />
        </View>
        <View style={styles.buttonWrapper}>
          <MenuButton
            color="#F9D9D9"
            name="airplane"
            title="PESAWAT"
            iconColor="#974141"
          />
          <MenuButton
            color="#FEF0C8"
            name="train"
            title="KERETA"
            iconColor="#E0B23E"
          />
          <MenuButton
            color="#BAE1FB"
            name="office-building"
            title="HOTEL"
            iconColor="#78B9EB"
          />
          <MenuButton
            color="#FADEDE"
            name="dots-horizontal"
            title="LAINNYA"
            iconColor="#CB8586"
          />
        </View>
        <View style={styles.bannerWrapper2}>
          <ScrollView
            key={3}
            onScroll={({nativeEvent}) => onChangeSecond(nativeEvent)}
            showsHorizontalScrollIndicator={false}
            pagingEnabled
            horizontal
            style={styles.bannerScroll}>
            {imagesBanner.map((e, index) => (
              <Image
                key={e}
                style={styles.bannerContent2}
                source={e.img}></Image>
            ))}
          </ScrollView>
          <View style={styles.wrapDot2}>
            {imagesBanner.map((e, index) => (
              <Text
                key={e}
                style={
                  imgActive2 == index ? styles.dotActive : styles.dotInActive
                }>
                ●
              </Text>
            ))}
          </View>
        </View>
        <View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              margin: 16,
            }}>
            <Text style={{fontSize: 20, fontWeight: 'bold'}}>Super Deal</Text>
            <TouchableOpacity>
              <Text style={{fontSize: 14, color: 'red'}}>
                lihat selengkapnya
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{flexDirection: 'row', marginBottom: 30}}>
            <ScrollView
              key={4}
              showsHorizontalScrollIndicator={false}
              pagingEnabled
              horizontal>
              {productList.map((data, index) => {
                return (
                  <ProductHome
                    color="white"
                    source={data.images[0]}
                    productName={data.name}
                    shopName={data.vendorResponse.name}
                    channelName={data.vendorResponse.picName}
                    onPress={() => onSelectProduct(data.id)}
                    key={data.id}></ProductHome>
                );
              })}
            </ScrollView>
          </View>
        </View>
        <View style={{backgroundColor: '#E53630'}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              margin: 16,
            }}>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>
              Promo Spesial
            </Text>
            <TouchableOpacity>
              <Text style={{fontSize: 14, color: 'white'}}>
                lihat selengkapnya
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{flexDirection: 'row', marginBottom: 30}}>
            <ScrollView
              key={5}
              showsHorizontalScrollIndicator={false}
              pagingEnabled
              horizontal>
              {productList.map((data, index) => {
                return (
                  <ProductHome
                    color="white"
                    source={data.images[0]}
                    productName={data.name}
                    onPress={() =>
                      navigation.navigate('DetailProduct', {
                        screen: 'Home',
                        params: {productId: data.id},
                      })
                    }
                    shopName={data.vendorResponse.name}
                    channelName={data.vendorResponse.picName}
                    key={data.id}></ProductHome>
                );
              })}
            </ScrollView>
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  topBar: {
    // width: SIZES.width,
    // height: SIZES.height * 0.12,
    backgroundColor: 'white',
    paddingLeft: 16,
    paddingRight: 16,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    shadowColor: '#000000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.9,
    shadowRadius: 3,
    elevation: 10,
    position: 'absolute',
    left: 0,
    right: 0,
  },

  topbarComponent: {
    borderRadius: 10,
    width: '80%',
    height: '60%',
    backgroundColor: '#E8E8E8',
  },

  bannerWrapper: {
    // backgroundColor: 'red',
    width: SIZES.width,
    // position: 'relative',
    // width: SIZES.width,
    height: SIZES.height * 2,
    // width: SIZES.width,
    // paddingVertical: 12,
    // paddingHorizontal: 12,
    // flex: 3,
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: 88,
    width: SIZES.width,
    height: SIZES.height * 0.25,
    // marginRight: 200
  },

  bannerScroll: {
    borderRadius: 10,
    // padding: 12,
    // marginTop: 12
  },

  bannerContent: {
    borderRadius: 10,
    height: SIZES.height * 0.25,
    width: SIZES.width,
    marginLeft: 12,
    // marginRight: 12,
    // paddingRight: 12,
    justifyContent: 'center',
    // alignSelf: 'center',
    // justifyContent: 'center',
    // width: '100%',
    // height: '100%',
    // flex: 1

    // resizeMode: 'cover',
    // padding: 12
    // margin: 12
  },

  wrapDot: {
    position: 'absolute',
    bottom: 0,
    // top: 270,
    paddingLeft: '30%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    // marginBottom: 90,
    zIndex: 9,
  },

  wrapDot2: {
    position: 'absolute',
    // bottom: "70%",
    bottom: 10,
    flexDirection: 'row',
    alignSelf: 'center',
    // marginBottom: 90,
    zIndex: 10,
  },

  dotActive: {
    margin: 3,
    color: 'black',
  },

  dotInActive: {
    margin: 3,
    color: 'white',
  },

  balanceWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#E53630',
    paddingRight: 24,
    paddingLeft: 24,
    margin: 12,
    borderRadius: 10,
    height: SIZES.height * 0.09,
  },

  balanceText: {
    color: 'white',
    fontSize: 14,
  },

  balanceMoneyText: {
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold',
    paddingRight: 8,
  },

  balanceWrapper2: {
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#E53630',
    paddingRight: 24,
    paddingLeft: 24,
    margin: 12,
    borderRadius: 10,
    height: SIZES.height * 0.15,
  },

  UserText: {
    color: 'white',
    fontSize: 14,
  },

  userBalance: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-around',
    alignItems: 'flex-start',
  },

  buttonWrapper: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },

  bannerWrapper2: {
    // backgroundColor: 'red',
    width: SIZES.width,
    // position: 'relative',
    // width: SIZES.width,
    // width: SIZES.width,
    // paddingVertical: 12,
    // paddingHorizontal: 12,
    // flex: 3,
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: 16,
    width: SIZES.width,
    height: SIZES.height * 0.25,
    // marginRight: 200
  },

  bannerContent2: {
    borderRadius: 10,
    height: SIZES.height * 0.25,
    width: SIZES.width,
    justifyContent: 'center',
    // alignSelf: 'center',
    // justifyContent: 'center',
    // width: '100%',
    // height: '100%',
    // flex: 1

    // resizeMode: 'cover',
    // padding: 12
    // margin: 12
  },
});
