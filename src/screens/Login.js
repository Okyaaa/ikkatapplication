import React, { useContext, useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  Platform,
  StyleSheet,
  ScrollView,
  ActivityIndicator,
} from "react-native";
import FormInputs from "../components/FormInput";
import FormInput from "../components/FormInput";
import Submit from "../components/Submit";
import { SIZES } from "../constants";
// import { AuthContext } from '../context/AuthContext';
import { useNavigation } from "@react-navigation/native";
import axios from "axios";
import { BASE_URL, REQRES, XSignature } from "../config";
import Icon from "react-native-vector-icons/FontAwesome5";
import { sha256 } from "react-native-sha256";

let otk = "";

const LoginScreen = () => {
  const navigation = useNavigation();
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  // const [otk, setOtk] = useState('');
  // const {isLoading, login} = useContext(AuthContext);
  const getLogin = async () => {
    axios
      .post(
        `https://demo.commerce.simpool.id/mp-master-wscustomer/service/login`,
        {
          email: email,
          password: tempMergePassword,
          source: "WEB",
          sourceDesc: "cent.com",
        },
        {
          headers: {
            "Content-Type": "application/json",
            "X-Signature": XSignature,
          },
        }
      )
      .then((response) => {
        console.log(response.data.errorMsg);
      })
      .catch((e) => {
        console.log(`login error ${e}`);
        alert("Input username atau password dengan benar");
      });
  };

  async function shaPassword(password, otk) {
    let tempSha = await sha256(password);
    let tempMergePassword = await sha256(`${tempSha}${otk}`);

    return tempMergePassword;
  }

  const requestOtk = async () => {
    try {
      let getOtk = await axios.post(
        `https://demo.commerce.simpool.id/mp-master-wscustomer/service/requestOtk`,
        {
          email: email,
        },
        {
          headers: {
            "Content-Type": "application/json",
            "X-Signature": XSignature,
          },
        }
      );
      otk = getOtk.data.otk;
    } catch (error) {
      console.log(error);
    }
  };

  const onSignUpPressed = () => {
    console.log("go to SignUp");
    navigation.navigate("SignUp");
  };

  const onSignInPressed = async () => {
    if (email === null || password == null) {
      alert("Email or password empty");
    } else {
      await requestOtk();
      console.log("otk = ", otk);
      let tempMergePassword = await shaPassword(password, otk);
      getLogin();
      navigation.navigate("Home");
    }
  };

  return (
    <ScrollView
      style={{ backgroundColor: "white" }}
      contentContainerStyle={{ flexGrow: 1, justifyContent: "center" }}
    >
      {/* <Spinner visible={isLoading}></Spinner> */}
      <View style={[styles.container, { flex: 1 }]}>
        <Image
          source={require("../assets/images/loginpage.jpg")}
          resizeMode="center"
          style={styles.image}
        />
        <Text style={styles.textTitle}>Selamat datang</Text>
        <Text style={styles.textBody}>
          Silahkan masuk menggunakan akun anda
        </Text>
        <View style={{ marginTop: 20 }} />

        <FormInput name="Email" icon="user" value={(text) => setEmail(text)} />
        <FormInput
          name="Password"
          icon="lock"
          pass={true}
          value={(text) => setPassword(text)}
        />
        <View style={{ width: "90%" }}>
          <Text style={([styles.textBody], { alignSelf: "flex-end" })}>
            Lupa password ?
          </Text>
        </View>

        <TouchableOpacity
          onPress={onSignInPressed}
          style={[styles.containerButton, { backgroundColor: "red" }]}
        >
          <Text style={styles.submitText}>Login</Text>
        </TouchableOpacity>

        <View style={{ flexDirection: "row", marginVertical: 5 }}>
          <Text style={styles.textBody}>TIdak memiliki akun ? </Text>
          <TouchableOpacity onPress={onSignUpPressed}>
            <Text style={[styles.textBody, { color: "blue" }]}>Daftar</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  image: {
    width: 400,
    height: 250,
    marginVertical: 10,
  },
  textTitle: {
    fontSize: 40,
    marginVertical: 10,
    fontWeight: "bold",
  },
  textBody: {
    fontSize: 16,
    marginVertical: 10,
  },
  containerButton: {
    width: "90%",
    height: 50,
    borderRadius: 10,
    marginVertical: 10,
    borderWidth: 0,
  },
  submitText: {
    fontSize: 22,
    fontWeight: "bold",
    color: "white",
    alignSelf: "center",
    marginVertical: 10,
  },
});

export default LoginScreen;
