import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Animated,
  Image,
} from "react-native";

import SafeAreaView from "react-native-safe-area-view";

//constants
import { COLORS, images, theme } from "../constants";
import Login from "./Login";
const { onboarding1, onboarding2, onboarding3 } = images;

//theme
const { SIZES } = theme;

//dummy data
const onBoardings = [
  {
    title: "onboarding1",
    description:
      "Description onboarding onboarding onboarding onboarding onboarding onboarding 1",
    img: onboarding1,
  },
  {
    title: "onboarding2",
    description:
      "Description onboarding onboarding onboarding onboarding onboarding onboarding 2",
    img: onboarding2,
  },
  {
    title: "onboarding3",
    description:
      "Description onboarding onboarding onboarding onboarding onboarding onboarding 3",
    img: onboarding3,
  },
];

const OnBoardingScreen = ({ navigation }) => {
  const [completed, setCompleted] = React.useState(false);
  const scrollX = new Animated.Value(0);

  React.useEffect(() => {
    scrollX.addListener(({ value }) => {
      if (Math.floor(value / SIZES.width) == onBoardings.length - 1) {
        setCompleted(true);
      }
    });
    return () => scrollX.removeListener();
  });

  function renderContent() {
    return (
      <Animated.ScrollView
        horizontal
        pagingEnabled
        scrollEnabled
        decelerationRate={10}
        scrollEventThrottle={16}
        snapToAlignment="center"
        showsHorizontalScrollIndicator={false}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { x: scrollX } } }],
          { useNativeDriver: false }
        )}
      >
        {onBoardings.map((item, index) => (
          <View key={index} style={{ width: SIZES.width }}>
            {/* {image} */}
            <View
              style={{
                flex: 1,
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Image
                source={item.img}
                resizeMode="contain"
                style={{
                  width: 350,
                  height: 350,
                }}
              />
            </View>

            {/* {Text} */}
            <View
              style={{
                bottom: "20%",
              }}
            >
              <Text
                style={{
                  color: COLORS.grey,
                  fontWeight: "bold",
                  fontSize: 36,
                  textAlign: "center",
                }}
              >
                {item.title}
              </Text>
              <Text
                style={{
                  color: COLORS.grey,
                  fontSize: 14,
                  textAlign: "center",
                }}
              >
                {item.description}
              </Text>
            </View>
            <TouchableOpacity
              style={{
                alignContent: "center",
                justifyContent: "center",
                bottom: "8%",
              }}
              onPress={() => navigation.navigate("Login")}
            >
              <Text
                style={{
                  textAlign: "center",
                }}
              >
                {completed ? "MULAI" : "LEWATI"}
              </Text>
            </TouchableOpacity>
          </View>
        ))}
      </Animated.ScrollView>
    );
  }

  function renderDots() {
    const dotPosition = Animated.divide(scrollX, SIZES.width);

    return (
      <View style={styles.dotContainer}>
        {onBoardings.map((item, index) => {
          const opacity = dotPosition.interpolate({
            inputRange: [index - 1, index, index + 1],
            outputRange: [0.3, 1, 0.3],
            extrapolate: "clamp",
          });

          const dotSize = dotPosition.interpolate({
            inputRange: [index - 1, index, index + 1],
            outputRange: [SIZES.base, 12, SIZES.base],
            extrapolate: "clamp",
          });

          return (
            <Animated.View
              key={`dot-${index}`}
              opacity={opacity}
              style={[styles.dot, { width: dotSize, height: dotSize }]}
            ></Animated.View>
          );
        })}
      </View>
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      <View>{renderContent()}</View>
      <View style={styles.dotRootContainer}>{renderDots()}</View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: COLORS.white,
  },
  dotRootContainer: {
    position: "absolute",
    bottom: SIZES.height > 700 ? "15%" : "1%",
  },
  dotContainer: {
    flexDirection: "row",
    height: SIZES.padding,
    alignItems: "center",
    justifyContent: "center",
  },
  dot: {
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.grey,
    marginHorizontal: SIZES.radius / 2,
  },
});

export default OnBoardingScreen;
