import axios from 'axios';
import React, {useState, useContext, useEffect, Fragment} from 'react';
import {
  View,
  StyleSheet,
  Text,
  ScrollView,
  Image,
  Systrace,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import Dropdown from '../components/Dropdown';
import FormInputs from '../components/FormInput';
import Submit from '../components/Submit';
import {BASE_URL, XSignature} from '../config';
import {SIZES} from '../constants';
import {Input} from 'react-native-elements';
import DropDownCity from '../components/DropdownCity';
import {sha256} from 'react-native-sha256';

const SignUp = ({props, navigation}) => {
  const [fullname, setFullName] = useState(null);
  const [email, setEmail] = useState(null);
  const [phone, setPhone] = useState(null);
  const [postalCode, setPostalCode] = useState(null);
  const [password, setPassword] = useState(null);
  const [cityData, setCityData] = useState([]);
  const [provinceData, setProvinceData] = useState([]);
  // const [tempSha, setTempSha] = useState('');

  const getDataProvince = async () => {
    axios
      .post(`${BASE_URL}/service/province/list`, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      .then(response => {
        console.log(response.data.list);
        setProvinceData(response.data.list);
      })
      .catch(e => {
        alert(e.message);
      });
  };

  const getDataCity = async item => {
    axios
      .post(
        `${BASE_URL}/service/city/list`,
        {
          source: 'WEB',
          sourceDesc: 'mp-master.com',
          provinceId: item.id,
        },
        {
          headers: {
            'Content-Type': 'application/json',
          },
        },
      )
      .then(response => {
        console.log(response.data.list);
        setCityData(response.data.list);
      })
      .catch(e => {
        alert(e.message);
      });
  };

  const [selectedItemProvince, setSelectedItemProvince] = useState(null);
  const [selectedItemCity, setSelectedItemCity] = useState(null);
  const onSelectProvince = item => {
    setSelectedItemProvince(item);
    getDataCity(item);
    console.log('selected provinsi = ', item);
    console.log('selected provinsi set :', selectedItemProvince);
  };
  const onSelectCity = item => {
    console.log('selected city = ', item);
    setSelectedItemCity(item);
  };

  const onSignUpPressed = async () => {
    console.log('SignUp');
    let temp = await sha256(password);
    console.log('outside = ', temp);
    console.log(`password = ${password}`);
    console.log(`email = ${email}`);
    axios
      .post(
        'https://demo.commerce.simpool.id/mp-master-wscustomer/service/reg/register',
        {
          name: fullname,
          email: email,
          phone: phone,
          address: provinceData.name,
          cityId: cityData.id,
          postalCode: postalCode,
          password: temp,
          source: 'mobile',
          urlActv: '-',
          emailAktifasi: 'no',
          comunityCode: 'simpool',
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'X-Signature': XSignature
          },
        },
      )
      .then(response => {
        console.log(response.status);
        console.log(response.data.errorMsg);
        navigation.navigate('Login');
      })
      .catch(e => {
        console.log(`login error ${e}`);
        alert('Input username atau password dengan benar');
      });
  };

  useEffect(() => {
    getDataProvince();
  }, []);

  return (
    <Fragment>
      <ScrollView
        style={{backgroundColor: 'white', alignContent: 'center'}}
        contentContainerStyle={{flexGrow: 1, justifyContent: 'center'}}>
        <View style={styles.container}>
          <FormInputs name="Nama Lengkap" value={text => setFullName(text)} />
          <FormInputs name="Email" value={text => setEmail(text)} />
          <FormInputs name="Phone" value={text => setPhone(text)} />
          <Dropdown
            data={provinceData}
            value={selectedItemProvince}
            // onChangeText={(text) => searchFilter}
            onSelect={onSelectProvince}></Dropdown>
          {console.log('Provinsi = ', selectedItemProvince?.id)}

          <DropDownCity
            data={cityData}
            value={selectedItemCity}
            // onChangeText={(text) => searchFilter}
            onSelect={onSelectCity}></DropDownCity>

          <FormInputs name="Kode Pos" value={text => setPostalCode(text)} />

          <FormInputs
            name="Password"
            pass={true}
            value={text => setPassword(text)}
          />
          <TouchableOpacity
            onPress={onSignUpPressed}
            style={[styles.containerButton, {backgroundColor: 'red'}]}>
            <Text style={styles.submitText}>SignUp</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </Fragment>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
  },
  containerButton: {
    width: '90%',
    height: 50,
    borderRadius: 10,
    marginVertical: 10,
    borderWidth: 0,
  },
  submitText: {
    fontSize: 22,
    fontWeight: 'bold',
    color: 'white',
    alignSelf: 'center',
    marginVertical: 10,
  },
  textTitle: {
    fontSize: 40,
    marginVertical: 8,
  },
  textBody: {
    fontSize: 16,
    marginVertical: 4,
  },
});

export default SignUp;
