import { Dimensions } from "react-native";

const {width, height} = Dimensions.get("window");

export const COLORS = {
    grey: '#696969',
    white: '#ffffff',
  };

export const SIZES = {
    //global sizes
    base: 8,
    font: 14,
    radius: 12,
    padding: 24,

    //font size
    h1: 30,
    h2: 22,
    h3: 16,
    h4: 14,
    body: 30,
    body: 22,
    body: 16,

    //App dimensions
    width,
    height
};

const appTheme = {COLORS, SIZES}

export default appTheme;
