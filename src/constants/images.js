export const onboarding1 = require("../assets/images/onboarding1.jpg");
export const onboarding2 = require("../assets/images/onboarding2.jpg");
export const onboarding3 = require("../assets/images/onboarding3.jpg");
export const arrowback = require("../assets/images/Icon/arrowback.png");
export const banner1 = require("../assets/images/banner-home/banner-3.jpg");
export const banner2 = require("../assets/images/banner-home/banner-2.jpg");
export const banner3 = require("../assets/images/banner-home/banner-3.jpg");
export const starOutline = require("../assets/images/Icon/star_corner.png");
export const starFilled = require("../assets/images/Icon/star_filled.png");

export default {
    onboarding1,
    onboarding2,
    onboarding3,
    arrowback,
    banner1,
    banner2,
    banner3,
    starOutline,
    starFilled,
}
