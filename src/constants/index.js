import images from "./images";
import theme, {SIZES, COLORS} from "./theme";

export {images, theme, SIZES, COLORS};