import React, {Component} from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


const MenuButton = props => {
  return (
    <View style={{justifyContent: 'center', alignItems: 'center'}}>
      <TouchableOpacity
        onPress={props.onPress}
        style={[styles.container, {backgroundColor: props.color}]}>
        <Icon
        style={{alignItems: 'center', justifyContent: 'center',alignSelf: 'center' }}
          name={props.name}
          size={40}
          color={props.iconColor}
        />
      </TouchableOpacity>
      <Text style={styles.submitText}>{props.title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: 70,
    height: 70,
    borderRadius: 10,
    marginVertical: 4,
    borderWidth: 0,
    justifyContent: 'center',
  },
  submitText: {
    justifyContent: 'center',
    fontSize: 14,
    color: '#222222',
    alignSelf: 'center',
    fontWeight: '400'
    // marginVertical: 10,
  },
});

export default MenuButton;
