import React, {Component, useState} from 'react';
import {
  View,
  StyleSheet,
  Text,
  ScrollView,
  Image,
  Systrace,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Input} from 'react-native-elements';
import {images} from '../constants';
const {arrowback} = images;

const DropDownCity = ({
  data = [], 
  value = {},
  // search = {},
  // setFilterData =[],
  // setSearch = [],
  // onSearchEnter, 
  onSelect = () => {}
  }) => {
//   console.log('selected value = ', !!value);
  console.log('selected value city = ', value);
  const [showOption, setShowOption] = useState(false);

  // const searchFilter = text => {
  //   if (text) {
  //     const newData = data.filter(item => {
  //       const itemData = item.name ? item.name : ''.toUpperCase();
  //       const textData = text.toUpperCase();
  //       return itemData.indexOf(textData) > -1;
  //     });
  //     setFilterData(newData);
  //     setSearch(text);
  //   } else {
  //     setFilterData(masterData);
  //     setSearch(text);
  //   }
  // };

  const onSelectedItem = val => {
    setShowOption(false);
    onSelect(val);
    // console.log('val dropdown = ', val);
  };

  return (
    <View style={[styles.container, {borderColor: '#f08080'}]}>
      <TouchableOpacity
        style={styles.dropDownStyle}
        activeOpacity={0.8}
        onPress={() => setShowOption(!showOption)}>
        <Text style={styles.inputText}>
          {!!value ? value?.name : `City`}
        </Text>
        {/* <Input
          placeholder={!!value ? value?.name : `City`}
          inputContainerStyle={styles.inputContainer}
          onChangeText={(text) => text}
          inputStyle={styles.inputText}
          onPressIn={() => setShowOption(!showOption)}
        /> */}
        <Image
          source={arrowback}
          style={{
            opacity: 0.3,
            transform: [{rotate: showOption ? '180deg' : '0deg'}],
          }}></Image>
      </TouchableOpacity>
      {showOption && (
        <View style={styles.containerItem}>
          <ScrollView
            keyboardShouldPersistTaps="handled"
            // showsVerticalScrollIndicator={false}
          >
            {data.map((val, i) => {
              return (
                <TouchableOpacity
                  key={String(i)}
                  onPress={() => onSelectedItem(val)}
                  style={{
                    backgroundColor: value?.id == val.id ? 'pink' : '#f2f2f2',
                    paddingVertical: 8,
                    borderRadius: 4,
                    paddingHorizontal: 6,
                  }}>
                  <Text style={styles.textDropdown}>{val.name}</Text>
                </TouchableOpacity>
              );
            })}
          </ScrollView>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '90%',
    height: 50,
    borderRadius: 12,
    marginVertical: 10,
    borderWidth: 1,
    zIndex: 998,
  },
  inputText: {
    fontSize: 17,
    color: 'grey',
  },
  dropDownStyle: {
    marginLeft: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 12,
    // maxWidth: "85%"
  },
  containerItem: {
    height: 200,
  },
  textDropdown: {
    marginLeft: 10,
    fontSize: 17,
    color: 'grey',
  },
  inputContainer: {
    borderBottomWidth: 0,
  },
});

export default DropDownCity;
