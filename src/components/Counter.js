import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const Counter = props => {
  return (
    <TouchableOpacity style={styles.roundButton} onPress={props.handleChange} disabled={props.disabled}>
      <Icon name={props.name} size={20} style={{color:'#E53630'}}></Icon>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  roundButton: {
    padding: 0.2,
    borderRadius: 100,
    borderColor:'#E53630',
    borderWidth: 2,
  },
});

export default Counter;
